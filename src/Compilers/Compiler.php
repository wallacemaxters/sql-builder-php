<?php

namespace PHPLegends\Database\Compilers;

use PHPLegends\Database\Raw;
use PHPLegends\Database\Builder;

class Compiler
{
    const DATETIME_FORMAT = 'Y-m-d H:i:s';

    const WHERE_TYPE_COMMON = 'Common';

    const WHERE_TYPE_IN = 'In';

    const WHERE_TYPE_IN_SUB = 'InSub';

    const WHERE_TYPE_NESTED = 'Nested';

    const WHERE_TYPE_RAW = 'Raw';

    public function compileInsert(Builder $builder, array $values)
    {
        $table = $builder->getFrom();

        if (! $table) {

            throw new UnexpectedValueException(
                'The "table" value is required in INSERT clause'
            );
        }

        $fields = $this->columnize(array_keys($values));

        $values = $this->parametrize($values);

        return "INSERT INTO {$table} ({$fields}) VALUES ({$values})";
    }

    public function compileSelect(Builder $builder)
    {
        $sql = sprintf(
            'SELECT %s FROM %s WHERE 1 = 1', 
            $this->columnize($builder->getSelect()),
            $builder->getFrom()
        );

        $sql .= $this->buildClausules($builder, ['Where', 'GroupBy', 'OrderBy', 'Limit']);

        return $sql;
    }

    public function compileDelete(Builder $builder)
    {
        $sql = "DELETE FROM {$builder->getFrom()} WHERE 1 = 1";

        $sql .= $this->buildClausules($builder, ['Where', 'OrderBy', 'Limit']);

        return $sql;

    }

    public function compileUpdate(Builder $builder, array $values)
    {
        $cb = function ($key, $value) {
            return $key . ' = ' . $this->parameter($value);
        };

        $set = implode(' ', array_map($cb, array_keys($values), $values));

        $sql = "UPDATE {$builder->getFrom()} SET {$set} WHERE 1 = 1";

        $sql .= $this->buildClausules($builder, ['Where', 'OrderBy', 'Limit']);

        return $sql;

    }

    /**
     * Compile the Where part of sql query builder
     * 
     * @param Builder $builder
     * @return string|null
     * */
    protected function compileWhere(Builder $builder)
    {
        $wheres = $builder->getWheres();

        if (! $wheres) return null;

        $expressions = [];

        foreach ($wheres as $where) {

            $method = 'where' . ucfirst($where['type']);

            $expressions[] = $this->$method($where);
        }

        return implode(' ', $expressions);
    }

    protected function whereCommon(array $where)
    {
        $value = $this->parameter($where['value']);

        return $this->getConnector($where['or']) . ' ' . $where['field'] . ' ' . $where['operator'] . ' ' . $value;
    }

    protected function whereIn(array $where)
    {
        $values = (array) $where['value'];

        $sqlPart = '1 = 0';

        $operator = $where['not'] ? 'NOT IN' : 'IN';

        if ($values) {

            $sqlPart = $where['field'] .
                       ' ' . $operator .
                       ' (' . $this->parametrize($values) . ')';
        }

        return $this->getConnector($where['or']) . ' ' . $sqlPart;
    }

    protected function whereInSub(array $where)
    {
        $query = $where['builder']->selectQuery();

        $operator = $where['not'] ? 'NOT IN' : 'IN';

        $sqlPart = "{$where['field']} {$operator} ({$query})";

        return$this->getConnector($where['or']) . ' ' . $sqlPart;
    }

    protected function whereNested(array $where)
    {
        $compiledWhere = $this->compileWhere($where['builder']);        

        if (! $compiledWhere) return '';

        return $this->getConnector($where['or']) . ' (1 = 1 ' . $compiledWhere . ')';
    }

    protected function whereRaw(array $where)
    {
        return $this->getConnector($where['or']) . ' ' . $where['expression'];
    }

    protected function compileOrderBy(Builder $builder)
    {
        $orderBy = $builder->getOrderBy();

        if (! $orderBy) return '';

        $direction = $orderBy['descending'] ? 'DESC' : 'ASC';

        return "ORDER BY {$orderBy['column']} {$direction}";
    }

    protected function compileLimit(Builder $builder)
    {
        
        if (! $limit = $builder->getLimit()) return null;

        $sql = 'LIMIT ' . $limit['limit'];

        if ($limit['offset']) {

            $sql .= ', ' . $limit['offset'];
        }

        return $sql;
    }

    /**
     * Compile the Group By clausule
     * 
     * @param Builder $builder
     * @return string|null
     * */
    protected function compileGroupBy(Builder $builder)
    {
        if (! $columns = $builder->getGroupBy()) return null;

        return "GROUP BY {$this->columnize($columns)}";
    }

    protected function parametrize(array $values)
    {
        return implode(', ', array_map([$this, 'parameter'], $values));
    }

    protected function parameter($value)
    {
        return $value instanceof Raw ? (string) $value : '?';
    }

    protected function columnize(array $columns)
    {
        return implode(', ', $columns);
    }

    protected function columnizeWithTable($column, $table)
    {
        return $this->columnize(array_map(function ($column) use ($table) {
            return $table . '.' . $column;
        }, $columns));
    }

    protected function buildClausules(Builder $builder, array $clausules)
    {
        $parts = null;

        foreach ($clausules as $clausule) {

            $method = 'compile' . $clausule;

            if ($part = $this->$method($builder)) $parts .= ' ' . $part;
        }

        return $parts;
    }

    protected function getConnector($isOr = false)
    {
        return $isOr ? 'OR' : 'AND';
    }
}