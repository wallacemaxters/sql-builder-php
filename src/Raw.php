<?php

namespace PHPLegends\Database;

class Raw
{
    protected $value;

    /**
     * 
     * @param mixed $value
     * */
    public function __construct($value)
    {
        $this->value = $value;
    }
    
    /**
     * 
     * @return mixed
     * */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * 
     * @return string
     * */
    public function __toString()
    {
        return (string) $this->getValue();
    }
}