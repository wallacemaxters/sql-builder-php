<?php

namespace PHPLegends\Database;

class Query extends Builder
{
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;

        parent::__construct($connection->getCompiler());
    }

    /**
     * @return PHPLegends\Database\Connection
     * */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * 
     * @return array
     * */
    public function get()
    {
        return $this->getConnection()->fetchAllResultsFromBuilder($this);
    }

    /**
     * Return the one value
     * 
     * @return array|null
     * */
    public function first()
    {
        $result = $this->limit(1)->get();

        return count($result) > 0 ? reset($result) : null;
    }

    /**
     * 
     * @return array|null
     * */
    public function find($value, $field = 'id')
    {
        return $this->where($field, '=', $value)->first();
    }

    /**
     * 
     * @return self
     * */
    public function table($name)
    {
        return $this->from($name);
    }

    /**
     * @param array $values
     * */
    public function insert(array $values)
    {
        $sql = $this->insertQuery($values);

        $this->connection->getPeparedStatement($sql, array_values($values));

        return $this->connection->getPdo()->lastInsertId();
    }


    /**
     * Updates a data and returns number of affected rows
     * 
     * @param array $values
     * @return int
     * */
    public function update(array $values)
    {
        $sql = $this->updateQuery($values);        

        $bindings = array_values(array_merge($values, $this->getPreparedBindings()));

        $stmt = $this->connection->getPeparedStatement($sql, $bindings);

        return $stmt->rowCount();
    }

    /**
     * Perform a delete query and return the affected rows
     * 
     * @return int
     * */
    public function delete() 
    {
        $sql = $this->deleteQuery();

        $stmt = $this->connection->getPeparedStatement($sql, $this->getPreparedBindings());

        return $stmt->rowCount();
    }


    public function count()
    {
        $result = $this->select(['COUNT(*) as __count__'])->first();

        return $result ? $result['__count__'] : 0;
    }

    /**
     * 
     * */
    public function newQuery()
    {
        return new static($this->connection);
    }
}