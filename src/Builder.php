<?php

namespace PHPLegends\Database;

use PHPLegends\Database\Compilers\Compiler;

/**
 * Representa o montador de querys
 * 
 * */
class Builder
{   
    /**
     * @var PHPLegends\Database\Compilers\Compiler
     * */
    protected $compiler;

    /**
     * @var array
     * */
    protected $select = ['*'];

    /**
     * The table name
     * @var string
     * */
    protected $from;

    /**
     * Array containing the where clausules
     * @var array
     * */
    protected $wheres = [];

    /**
     * @var array
     * */
    protected $orderBy = [];

    /**
     * @var array
     * */
    protected $limit = [];

    /**
     * @var array
     * */
    protected $bindings = [
        'where' => [],
        'join'  => [],
    ];

    /**
     * @var array
     * */
    protected $groupBy = [];

    public function __construct(Compiler $compiler = null)
    {
        $this->compiler = $compiler ?: new Compiler;
    }

    public function insertQuery(array $values)
    {
        return $this->compiler->compileInsert($this, $values);
    }

    public function deleteQuery()
    {
        return $this->compiler->compileDelete($this);   
    }

    public function selectQuery()
    {
        return $this->compiler->compileSelect($this);
    }

    public function updateQuery(array $values)
    {
        return $this->compiler->compileUpdate($this, $values);
    }

    public function select(array $select)
    {
        $this->select = $select;

        return $this;
    }

    public function from($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * 
     * */
    public function limit($limit, $offset = null)
    {
        $this->limit = compact('limit', 'offset');

        return $this;
    }

    /**
     * 
     * @param string $column
     * @param boolean $descending
     * @return self
     * */
    public function orderBy($column, $descending = false)
    {
        $this->orderBy = compact('column', 'descending');

        return $this;
    }  

    /**
     * 
     * @param string $field
     * @param string $operator
     * @param mixed $value
     * @return self
     * */
    public function where($field, $operator, $value, $or = false)
    {
        $this->wheres[] = [
            'field'     => $field,
            'operator'  => $operator,
            'or'        => $or,
            'value'     => $value,
            'type'      => Compiler::WHERE_TYPE_COMMON,
        ];

        $this->addBinding($value, 'where');

        return $this;
    }

    public function orWhere($field, $operator, $value)
    {
        return $this->where($field, $operator, $value, true);
    } 

    public function whereIn($field, array $values, $or = false, $not = false)
    {
        $this->wheres[] = [
            'field' => $field,
            'not'   => $not,
            'or'    => $or,
            'value' => $values,
            'type'  => Compiler::WHERE_TYPE_IN,
        ];

        $this->addBindings($values, 'where');

        return $this;
    }

    public function whereInSub($field, \Closure $closure, $not = false, $or = false)
    {
        $builder = $this->newQuery();

        $closure($builder);

        $this->mergeBindings($builder);

        $this->wheres[] = [
            'field'      => $field,
            'not'        => $not,
            'or'         => $or,
            'type'       => Compiler::WHERE_TYPE_IN_SUB,
            'builder'    => $builder,
        ];

        return $this;
    }

    public function orWhereIn($field, $values)
    {
        return $this->whereIn($field, $values, true);
    }

    public function whereNotIn($field, $values, $or = false)
    {   
        return $this->whereIn($field, $values, $or, true);
    }

    public function orWhereNotIn($field, $values)
    {
        return $this->whereNotIn($field, $values, true);
    }

    public function whereRaw($expression, $or = false)
    {
        $expression instanceof Raw ?: $expression = new Raw($expression);

        $this->wheres[] = [
            'expression' => $expression,
            'type'       => Compiler::WHERE_TYPE_RAW,
            'or'         => $or
        ];

        return $this;
    }

    public function whereNested(\Closure $closure, $or = false)
    {
        $builder = $this->newQuery();

        $closure($builder);

        $this->mergeBindings($builder);

        $this->wheres[] = [
            'or'      => $or,
            'type'    => Compiler::WHERE_TYPE_NESTED,
            'builder' => $builder,
        ];

        return $this;
    }

    /**
     * 
     * Grouped WHERE clause wrapped by ()
     * 
     * @param array $values
     * @return self
     * */
    public function whereAre(array $values, $or = false)
    {
        return $this->whereNested(function ($query) use ($values) {

            foreach ($values as $name => $value) {

                $query->where($name, '=', $value);
            }

        }, $or);
    }

    /**
     * 
     * Grouped OR WHERE clause wrapped by ()
     * 
     * @param array $values
     * @return self
     * */
    public function orWhereAre(array $values)
    {
        return $this->whereAre($values, true);
    }

    /**
     * 
     * @param string|array $columns
     * @return self
     * */
    public function groupBy($columns)
    {
        $this->groupBy = (array) $columns;

        return $this;
    }

    public function newQuery()
    {
        return new static($this->compiler);
    }

    /**
     * Gets the value of select.
     *
     * @return mixed
     */
    public function getSelect()
    {
        return $this->select;
    }

    /**
     * Gets the value of from.
     *
     * @return string
     */
    public function getFrom()
    {
        if ($this->from === null) {
            throw new \RuntimeException("The table value cannot be NULL");
        }

        return $this->from;
    }

    /**
     * Gets the value of wheres.
     *
     * @return mixed
     */
    public function getWheres()
    {
        return $this->wheres;
    }

    /**
     * Gets the value of orderBy.
     *
     * @return mixed
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * Gets the value of limit.
     *
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }


    /**
     * Gets the value of compiler.
     *
     * @return PHPLegends\Database\Compilers\Compiler
     */
    public function getCompiler()
    {
        return $this->compiler;
    }    

    public function getBindings()
    {
        return $this->bindings;
    }

    public function flattenBindings()
    {
        return call_user_func_array('array_merge', $this->bindings);
    }

    public function getPreparedBindings()
    {
        return $this->prepareBindings($this->flattenBindings());
    }

    public function mergeBindings(self $builder)
    {
        $this->bindings = array_merge_recursive($this->getBindings(), $builder->getBindings());

        return $this;
    }

    protected function addBinding($value, $type)
    {
        if (! $value instanceof Raw) {

            $this->bindings[$type][] = $value;
        }

        return $this;
    }

    protected function addBindings(array $values, $type)
    {
        foreach ($values as $value) {

            $this->addBinding($value, $type);
        }

        return $this;
    }


    /**
     * Gets the value of groupBy.
     *
     * @return array
     */
    public function getGroupBy()
    {
        return $this->groupBy;
    }

    /**
     * Prepare bindings
     * 
     * @param array $bindings
     * @return array
     * */
    public function prepareBindings(array $bindings)
    {
        $compiler = $this->compiler;
        
        return array_map(static function ($binding) use ($compiler) {

            if ($binding instanceof \DateTime) {
                return $binding->format($compiler::DATETIME_FORMAT);
            }

            return $binding;

        }, $bindings);
    }
}