<?php

namespace PHPLegends\Database;

use PHPLegends\Database\Builder;
use PHPLegends\Database\Compilers\Compiler;

class Connection
{
    protected $pdo;

    /**
     * 
     * */
    protected $compiler;

    public function __construct(\PDO $pdo, Compiler $compiler = null)
    {
        $this->pdo = $pdo;
        $this->compiler = $compiler;
    }

    /**
     * Gets the value of pdo.
     *
     * @return \PDO
     */
    public function getPdo()
    {
        return $this->pdo;
    }

    /**
     * 
     * @return PHPLegends\Database\Compilers\Compiler
     * */

    public function getCompiler()
    {
        return $this->compiler;
    }

    /**
     * 
     * @param array $config
     * @return 
     * */
    public static function factory(array $config)
    {

        if (preg_match('/^sqlite/', $config['driver']) > 0) {

            $dsn = $config['driver'] . ':' . $config['database'];

            $pdo = new \PDO($dsn);
            
        } else {

            $dsn = sprintf(
                '%s:host=%s;port=%s;dbname=%s', $config['driver'], $config['host'], $config['port'], $config['database']
            );


            $pdo = new \PDO($dsn, $config['username'], $config['password']);
        }

        return new static($pdo);
    }

    /**
     * @param string $sql
     * @param array $bindings
     * @return \PDOStatment
     * */
    public function getPeparedStatement($sql, array $bindings = [])
    {
        $stmt = $this->pdo->prepare($sql);

        $stmt->execute($bindings);

        return $stmt;
    }

    /**
     *
     * @return array
     * */
    public function fetchResultFromQuery($sql, array $bindings = [])
    {
        return $this->getPeparedStatement($sql, $bindings)->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     *
     * @return array
     * */
    public function fetchAllResultsFromQuery($sql, array $bindings = [])
    {
        return $this->getPeparedStatement($sql, $bindings)->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Executes a raw SQL query
     * 
     * @return mixed|array
     * */

    public function runQuery($sql)
    {
        return $this->pdo->query($sql, \PDO::FETCH_ASSOC);
    }

    /**
     * 
     * @return mixed
     * */
    public function fetchAllResultsFromBuilder(Builder $builder)
    {
        return $this->fetchAllResultsFromQuery($builder->selectQuery(), $builder->getPreparedBindings());
    }

    /**
     * 
     * @return mixed
     * */
    public function fetchResultFromBuilder(Builder $builder)
    {
        return $this->fetchResultFromQuery($builder->selectQuery(), $builder->getPreparedBindings());
    }


    /**
     * Create builder with the selected table
     * 
     * @return PHPLegends\Database\Builder
     * */
    public function fromTable($name)
    {
        return $this->createBuilder()->from($name);
    }

    /**
     * @return PHPLegends\Database\Builder
     * */
    public function createBuilder()
    {
        return new Builder($this->compiler);
    }
}