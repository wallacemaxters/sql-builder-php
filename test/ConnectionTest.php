<?php

use PHPLegends\Database\Builder;
use PHPLegends\Database\Connection;
use PHPLegends\Database\Compilers\Compiler;

class ConnectionTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->connection = Connection::factory([
            'driver'   => 'sqlite',
            'database' => __DIR__ . '/database.sqlite'
        ]);
    }

    public function testGetPdo()
    {
        $this->assertInstanceOf('PDO', $this->connection->getPdo());
    }

    public function testfetchResultFromQuery()
    {
        $result = $this->connection->fetchResultFromQuery('SELECT * FROM users WHERE id = ?', [1]);

        $this->assertTrue(isset($result['name']));

        $this->assertTrue(isset($result['id']));
    }


    public function testFetchResultFromBuilder()
    {
        $builder = new Builder();

        $builder->from('users')->select(['name'])->whereAre(['id' => 1])->limit(1);

        $result = $this->connection->fetchResultFromBuilder($builder);

        $this->assertTrue(isset($result['name']));

    }

    public function testCreateBuilder()
    {
        $this->assertInstanceOf('PHPLegends\Database\Builder', $this->connection->createBuilder());
    }

    public function testFromTable()
    {
        $builder = $this->connection->fromTable('users');

        $this->assertInstanceOf('PHPLegends\Database\Builder', $builder);

        $this->assertEquals('users', $builder->getFrom());
    }

}