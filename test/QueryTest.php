<?php

use PHPLegends\Database\Query;
use PHPLegends\Database\Connection;


class QueryTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $conn = Connection::factory([
            'driver'   => 'sqlite',
            'database' => __DIR__ . '/query_test_database.sqlite'
        ]);

        $this->query = new Query($conn);
    }

    public function testGetConnection()
    {
        $this->assertInstanceOf('PHPLegends\Database\Connection', $this->query->getConnection());
    }

    public function testGet()
    {
        $result = $this->query->from('users')->get();

        $this->assertTrue(is_array($result));
    }

    public function testFind()
    {
        $result = $this->query->from('users')->find(1);

        $this->assertTrue(is_array($result) || is_null($result));
    }

    public function testFirst()
    {
        $result = $this->query->from('users')->orderBy('id', 'desc')->first();

        $this->assertTrue(is_array($result) || is_null($result));
    }

    public function testInsert()
    {
        $this->query->from('users')->insert([
            'id'   => 3,
            'name' => 'Wayne'
        ]);

    }

    public function testUpdate()
    {
        $query = $this->query->from('users')->where('id', '=', 3)->limit(1);

        $data = [
            'name' => 'Wayne de Souza ' . uniqid()
        ];

        $result = $query->update($data);

        $this->assertTrue($result > 0);
    }


    public function testDelete()
    {
        $result = $this->query->from('users')->where('name', '=', 'Mafafacacacoco')->delete();

        $this->assertEquals(0, $result);

        $this->query->newQuery()->from('users')->insert(['name' => 'Jonhson']);

        $result = ($q = $this->query->newQuery()->from('users')->where('name', '=', 'Jonhson'))->delete();

        $this->assertEquals(1, $result);
    }
}