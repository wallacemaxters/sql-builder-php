<?php

use PHPLegends\Database\Raw;
use PHPLegends\Database\Builder;
use PHPLegends\Database\Compilers\Compiler;

class BuilderTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->builder = $this->createBuilder();
    }

    protected function createBuilder()
    {
        return new Builder(new Compiler);
    }

    public function testIn()
    {
        $sql = $this->builder->from('table')->whereIn('nome', [1, 2, 3])->selectQuery();

        $this->assertEquals(
            'SELECT * FROM table WHERE 1 = 1 AND nome IN (?, ?, ?)',
            $sql
        );
    }

    public function testOrIn()
    {
        $sql = $this->builder->from('table')->whereIn('nome', [1, 2, 3])->orWhereIn('id', [4, 5, 6])->selectQuery();

        $this->assertEquals(
            'SELECT * FROM table WHERE 1 = 1 AND nome IN (?, ?, ?) OR id IN (?, ?, ?)',
            $sql
        );
    }

    public function testInsub()
    {
         $sql = $this->builder->from('usuarios')->whereInSub('nivel_id', function ($query) {

            $query->from('niveis')
                  ->select(['id'])
                  ->whereIn('id', [1, 2, 3])
                  ->where('nome', 'LIKE', 'admin%');

         })->selectQuery();

         $this->assertEquals([1, 2, 3, 'admin%'], $this->builder->getPreparedBindings());

         $this->assertEquals(
            'SELECT * FROM usuarios WHERE 1 = 1 AND nivel_id IN (SELECT id FROM niveis WHERE 1 = 1 AND id IN (?, ?, ?) AND nome LIKE ?)',
            $sql
        );
    }

    public function testSub()
    {
        $sql = $this->builder->from('users')->whereNested(function ($query) {

            $query->where('name', 'LIKE', 'Gu%')
                    ->whereNested(function ($query) {
                        $query->where('idade', '=', 32);
                    });

        })->orWhere('status', '=', 1)->selectQuery();

       
       $this->assertEquals(
            'SELECT * FROM users WHERE 1 = 1 AND (1 = 1 AND name LIKE ? AND (1 = 1 AND idade = ?)) OR status = ?',
            $sql
        );

       $this->assertEquals(['Gu%', 32, 1], $this->builder->getPreparedBindings());
    }

    public function testUpdate()
    {
        $sql = $this->builder
                    ->from('users')
                    ->where('email', '=', 'lucas@gmail.com')
                    ->where('nome', '=', 'wallace')
                    ->updateQuery([
                        'email' => 'lili@gmail.com',
                        'user'  => 'Lili',
                    ]);

        $this->assertEquals('UPDATE users SET email = ? user = ? WHERE 1 = 1 AND email = ? AND nome = ?', $sql);
    }

    public function testSelectQuery()
    {
        $sql = $this->builder
                ->from('table')
                ->select(['nome', 'id'])
                ->where('nome', 'LIKE', 'wallace%')
                ->where('nivel_id', '=', '1');

        $this->assertEquals('SELECT nome, id FROM table WHERE 1 = 1 AND nome LIKE ? AND nivel_id = ?', $sql->selectQuery());

        $this->assertEquals(['wallace%', '1'], $this->builder->getPreparedBindings());

        $this->assertEquals(
            'SELECT nome, id FROM table WHERE 1 = 1 AND nome LIKE ? AND nivel_id = ? LIMIT 50, 5',
            $sql->limit(50, 5)->selectQuery()
        );

        $this->assertEquals(
            'SELECT nome, id FROM table WHERE 1 = 1 AND nome LIKE ? AND nivel_id = ? AND nome = "Wallace" LIMIT 50, 5',
            $sql->where('nome', '=', new Raw('"Wallace"'))->selectQuery()
        );

        $this->assertEquals(['wallace%', '1'], $this->builder->getPreparedBindings());
    }

    public function testGroupByAndOrderBy()
    {
        $sql = $this->builder
                    ->from('users')
                    ->where('name', '=', 'Jonh')
                    ->groupBy(['id', 'name'])
                    ->orderBy('id', true)
                    ->selectQuery();


        $this->assertEquals(
            'SELECT * FROM users WHERE 1 = 1 AND name = ? GROUP BY id, name ORDER BY id DESC',
            $sql
        );
    }

    public function testInsertQuery()
    {
        $result = $this->builder->from('users')->insertQuery([
            'name'  => 'Miguel',
            'email' => 'miguel.test@gmail.com'
        ]);

        $this->assertEquals(
            'INSERT INTO users (name, email) VALUES (?, ?)',
            $result
        );
    }

    public function testDeleteQuery()
    { 
        $result = $this->builder->from('users')->where('id', '=', 1)->deleteQuery();

        $this->assertEquals("DELETE FROM users WHERE 1 = 1 AND id = ?", $result);

    }

    public function testWhereAre()
    {
        $result = $this->builder
                    ->from('users')
                    ->select(['id', 'name'])
                    ->whereAre([
                        'name'  => 'Alice',
                        'email' => 'alice@example.com'
                    ])
                    ->limit(1)
                    ->selectQuery();

        $this->assertEquals(
            'SELECT id, name FROM users WHERE 1 = 1 AND (1 = 1 AND name = ? AND email = ?) LIMIT 1',
            $result
        );
    }


    public function testWhereWithDateTime()
    {
        $sql = $this->builder
                ->from('users')
                ->where('created_at', '>=', clone $date = new DateTime('2015-05-10 00:00:00'))
                ->where('created_at', '<=', $date->modify('+1 day'))
                ->selectQuery();

        $this->assertEquals(
            '2015-05-10 00:00:00',
            $this->builder->getPreparedBindings()[0]
        );

    }

    public function testWhereAreAndOrWhereAre()
    {
        $sql = $this->builder
                    ->from('users')
                    ->whereAre(['nome' => 'Wallace', 'email' => 'wallacemaxters@gmail.com'])
                    ->orWhereAre(['email' => 'gustavo.carmo@grupotmt.com.br'])
                    ->selectQuery();


        $this->assertEquals(
            'SELECT * FROM users WHERE 1 = 1 AND (1 = 1 AND nome = ? AND email = ?) OR (1 = 1 AND email = ?)',
            $sql
        );
    }
}
